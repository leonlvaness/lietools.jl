# KW 19
TODO:
- demazure tests
- tests allgemein

# Allgemeines TODO
- Jupyter Notebook
- WGLMakie?
- Besseres interface, Überladungen für Base in Graph-Methoden?
- HW-Modul-Datentyp: Dimension, Charakter, Crystal Base, Crystal Graph, Demazure Crystal, Demazure Modul?
- Ist Demazures Charakterformel also durch Summe über wt(b) für alle b aus dem Demazure Crystal gegeben?
- sl-crystal als gl(n+1) crystal?
- Cartan-Matrix transponieren? Mit HK vergleichen